package com.company;


import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {

    public static void main(String[] args) throws IOException {
        Main m = new Main();
        m.sendRequest();

    }

    private void sendRequest() {

        try {
            URL url = new URL("http://www.timeapi.org/eet/now");

            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("GET");

            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            String res;
            while ((res = br.readLine()) != null) {
                res=res.substring(res.indexOf('T')+1,res.indexOf('+'));
                System.out.print(res);
            }



            con.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
